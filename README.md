# COMP

COMP is a structured specification language and analysis tool that supports the formal development of component-based systems.
It is based on the behavioural-abstraction paradigm, which gives prominence to the observable behaviour of systems over their structural representations or the functions that they may perform.

The language is rigorously based upon [hidden algebra](https://cseweb.ucsd.edu/~goguen/projs/halg.html).
Its main programming units, called object modules, consist of hidden-algebra declarations of data or state sorts, data operations, actions, observations, and axioms that bring everything together and define the semantics of programs.
Structured specifications are obtained through parallel, synchronized, and indexed compositions of object modules – a defining feature of COMP – which allow the construction of larger and more complex objects, in a hierarchical fashion, from simple components.

Full documentation and examples are available on the [COMP website](http://www.imar.ro/~diacon/COMPproject/COMP.html).

## Obtaining COMP

COMP is implemented in [Maude 3](http://maude.cs.illinois.edu/w/index.php/The_Maude_System) and is integrated into the [SpeX](https://gitlab.com/ittutu/spex/) specification framework.
Therefore, in order to run COMP, you need to have both Maude version 3 and SpeX installed on your system.

The latest version of COMP is available [here](https://gitlab.com/ittutu/comp/-/raw/main/dist/comp-22.09.tar.gz).
On GNU/Linux machines, the simplest way to install it is by running the following scripts from the directory containing the COMP source tree:

```shell
./configure
make
sudo make install
```

## License

The COMP source code is licensed under the [GNU General Public License v2.0 or later](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html).

## Acknowledgements

This work was supported by a grant of the Romanian Ministry of Education and Research, CCCDI – UEFISCDI, project number PN-III-P2-2.1-PED-2019-0955, within PNCDI III.
